﻿namespace UpdatingCI_ItemCode_Dimensions
{
    partial class IC_ItemCode_Dimension_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.itemCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dIMDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cartonQuantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.f9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.largerQTYGDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cIItemCodeDimentionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.kencoveShippingDataSet = new UpdatingCI_ItemCode_Dimensions.KencoveShippingDataSet();
            this.cI_ItemCode_DimentionTableAdapter = new UpdatingCI_ItemCode_Dimensions.KencoveShippingDataSetTableAdapters.CI_ItemCode_DimentionTableAdapter();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.btn_refresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIItemCodeDimentionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kencoveShippingDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.itemCodeDataGridViewTextBoxColumn,
            this.dIMDataGridViewTextBoxColumn,
            this.lDataGridViewTextBoxColumn,
            this.wDataGridViewTextBoxColumn,
            this.hDataGridViewTextBoxColumn,
            this.cartonQuantityDataGridViewTextBoxColumn,
            this.bOXDataGridViewTextBoxColumn,
            this.f6DataGridViewTextBoxColumn,
            this.f7DataGridViewTextBoxColumn,
            this.f8DataGridViewTextBoxColumn,
            this.f9DataGridViewTextBoxColumn,
            this.typeDataGridViewTextBoxColumn,
            this.largerQTYGDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.cIItemCodeDimentionBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 32);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1343, 569);
            this.dataGridView1.TabIndex = 0;
            // 
            // itemCodeDataGridViewTextBoxColumn
            // 
            this.itemCodeDataGridViewTextBoxColumn.DataPropertyName = "ItemCode";
            this.itemCodeDataGridViewTextBoxColumn.HeaderText = "ItemCode";
            this.itemCodeDataGridViewTextBoxColumn.Name = "itemCodeDataGridViewTextBoxColumn";
            // 
            // dIMDataGridViewTextBoxColumn
            // 
            this.dIMDataGridViewTextBoxColumn.DataPropertyName = "DIM";
            this.dIMDataGridViewTextBoxColumn.HeaderText = "DIM";
            this.dIMDataGridViewTextBoxColumn.Name = "dIMDataGridViewTextBoxColumn";
            // 
            // lDataGridViewTextBoxColumn
            // 
            this.lDataGridViewTextBoxColumn.DataPropertyName = "l";
            this.lDataGridViewTextBoxColumn.HeaderText = "l";
            this.lDataGridViewTextBoxColumn.Name = "lDataGridViewTextBoxColumn";
            // 
            // wDataGridViewTextBoxColumn
            // 
            this.wDataGridViewTextBoxColumn.DataPropertyName = "w";
            this.wDataGridViewTextBoxColumn.HeaderText = "w";
            this.wDataGridViewTextBoxColumn.Name = "wDataGridViewTextBoxColumn";
            // 
            // hDataGridViewTextBoxColumn
            // 
            this.hDataGridViewTextBoxColumn.DataPropertyName = "h";
            this.hDataGridViewTextBoxColumn.HeaderText = "h";
            this.hDataGridViewTextBoxColumn.Name = "hDataGridViewTextBoxColumn";
            // 
            // cartonQuantityDataGridViewTextBoxColumn
            // 
            this.cartonQuantityDataGridViewTextBoxColumn.DataPropertyName = "CartonQuantity";
            this.cartonQuantityDataGridViewTextBoxColumn.HeaderText = "CartonQuantity";
            this.cartonQuantityDataGridViewTextBoxColumn.Name = "cartonQuantityDataGridViewTextBoxColumn";
            // 
            // bOXDataGridViewTextBoxColumn
            // 
            this.bOXDataGridViewTextBoxColumn.DataPropertyName = "BOX";
            this.bOXDataGridViewTextBoxColumn.HeaderText = "BOX";
            this.bOXDataGridViewTextBoxColumn.Name = "bOXDataGridViewTextBoxColumn";
            // 
            // f6DataGridViewTextBoxColumn
            // 
            this.f6DataGridViewTextBoxColumn.DataPropertyName = "F6";
            this.f6DataGridViewTextBoxColumn.HeaderText = "F6";
            this.f6DataGridViewTextBoxColumn.Name = "f6DataGridViewTextBoxColumn";
            // 
            // f7DataGridViewTextBoxColumn
            // 
            this.f7DataGridViewTextBoxColumn.DataPropertyName = "F7";
            this.f7DataGridViewTextBoxColumn.HeaderText = "F7";
            this.f7DataGridViewTextBoxColumn.Name = "f7DataGridViewTextBoxColumn";
            // 
            // f8DataGridViewTextBoxColumn
            // 
            this.f8DataGridViewTextBoxColumn.DataPropertyName = "F8";
            this.f8DataGridViewTextBoxColumn.HeaderText = "F8";
            this.f8DataGridViewTextBoxColumn.Name = "f8DataGridViewTextBoxColumn";
            // 
            // f9DataGridViewTextBoxColumn
            // 
            this.f9DataGridViewTextBoxColumn.DataPropertyName = "F9";
            this.f9DataGridViewTextBoxColumn.HeaderText = "F9";
            this.f9DataGridViewTextBoxColumn.Name = "f9DataGridViewTextBoxColumn";
            // 
            // typeDataGridViewTextBoxColumn
            // 
            this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
            this.typeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
            // 
            // largerQTYGDataGridViewTextBoxColumn
            // 
            this.largerQTYGDataGridViewTextBoxColumn.DataPropertyName = "LargerQTYG";
            this.largerQTYGDataGridViewTextBoxColumn.HeaderText = "LargerQTYG";
            this.largerQTYGDataGridViewTextBoxColumn.Name = "largerQTYGDataGridViewTextBoxColumn";
            // 
            // cIItemCodeDimentionBindingSource
            // 
            this.cIItemCodeDimentionBindingSource.DataMember = "CI_ItemCode_Dimention";
            this.cIItemCodeDimentionBindingSource.DataSource = this.kencoveShippingDataSet;
            // 
            // kencoveShippingDataSet
            // 
            this.kencoveShippingDataSet.DataSetName = "KencoveShippingDataSet";
            this.kencoveShippingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cI_ItemCode_DimentionTableAdapter
            // 
            this.cI_ItemCode_DimentionTableAdapter.ClearBeforeFill = true;
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(633, 625);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(102, 41);
            this.btn_Save.TabIndex = 1;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(1253, 625);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(102, 41);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "Close";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Location = new System.Drawing.Point(33, 625);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(106, 41);
            this.btn_refresh.TabIndex = 3;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Visible = false;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // IC_ItemCode_Dimension_Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1367, 678);
            this.Controls.Add(this.btn_refresh);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.dataGridView1);
            this.Name = "IC_ItemCode_Dimension_Update";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cIItemCodeDimentionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kencoveShippingDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private KencoveShippingDataSet kencoveShippingDataSet;
        private System.Windows.Forms.BindingSource cIItemCodeDimentionBindingSource;
        private KencoveShippingDataSetTableAdapters.CI_ItemCode_DimentionTableAdapter cI_ItemCode_DimentionTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn itemCodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dIMDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn hDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cartonQuantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bOXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn f6DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn f7DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn f8DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn f9DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn largerQTYGDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Button btn_refresh;
    }
}


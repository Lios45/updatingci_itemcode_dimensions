﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UpdatingCI_ItemCode_Dimensions
{
    public partial class IC_ItemCode_Dimension_Update : Form
    {
        
        public IC_ItemCode_Dimension_Update()
        {
            InitializeComponent();
            this.Text = "Update CI_ITemCode_Dimention";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'kencoveShippingDataSet.CI_ItemCode_Dimention' table. You can move, or remove it, as needed.
            this.cI_ItemCode_DimentionTableAdapter.Fill(this.kencoveShippingDataSet.CI_ItemCode_Dimention);
          // count = 0;
          //  foreach (DataGridViewRow row in dataGridView1.Rows)
          //  {
          //      count++;
          //  }
          //MessageBox.Show(count.ToString());  
        }

      
        private void btn_Save_Click(object sender, EventArgs e)
        {
          
               
                try
                 {
                //this.cI_ItemCode_DimentionTableAdapter.Fill()
                     this.Validate();
                    this.cI_ItemCode_DimentionTableAdapter.Update(this.kencoveShippingDataSet.CI_ItemCode_Dimention);
                     MessageBox.Show("Data Sucessfully Updated");
                     // this.kencoveShippingDataSet.CI_ItemCode_Dimention.AcceptChanges();
                 }
                catch (Exception ex)
                 {
                MessageBox.Show(ex.Message.ToString());
                }
            
           
            
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_refresh_Click(object sender, EventArgs e) //refrest button
        {
            this.cI_ItemCode_DimentionTableAdapter.Fill(this.kencoveShippingDataSet.CI_ItemCode_Dimention);
        }
    }
}
